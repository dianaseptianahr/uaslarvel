<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\AngkakematianModel;

class AngkakematianController extends Controller
{
    public function index(){
        $data = AngkakematianModel::all()->where('is_active',1) ;
        return view('kasus.Angkakematian',compact('data'));
    }

    public function TambahData(){
        return view('kasus.TambahAngkakematian');
    }

    public function postDataAngkakematian(Request $request){
        $simpan = DB::table('angkakematian')->insert([
            'id_Angkakematian' => $request->id_Angkakematian,
            'id_kasus' => $request->id_kasus,
            'jumlah' => $request->jumlah,
            'is_active'=> 1,
        ]);
        // dd($simpan);

        if(!$simpan){
            return redirect()->route('tampil_angkakematian')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_angkakematian')->with('success','data berhasil disimpan');
    }

    
    public function editAngkakematian($id ){
        $data = AngkakematianModel::where('id_kasus',$id)->first();        
        return view('kasus.EditAngkakematian', compact('data'));
    }

    public function updateAngkaKematian($id,Request $request ){
        $simpan =AngkakematianModel::where('id_angkakematian',$id)->update([
            'id_angkakematian' => $request->id_angkakematian,
            'id_kasus' => $request->id_kasus,
            'jumlah' => $request->jumlah,
            'is_active'=> 1,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_angkakematian')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_angkakematian')->with('succes','data berhasil disimpan');
        
    }

    // public function updateAngkaKematian($id,Request $request ){
    //     $data =AngkakematianModel::where('id',$id)->update([
    //         'id_kasus' => $request->id,
    //         'nama_kasus' => $request->nama,
    //         'faktor_kematian' => $request->nama,
    //         'jumlah' => $request->nama
    //     ]);

    //     return redirect('Angkakematian');
    // }

    public function softDelete($id,AngkakematianModel $orangModel){
        $orangModel->where('id_kasus',$id)->update([
            'is_active'=>'0']
        );

        return redirect('Angkakematian');
    }

    public function CariData(Request $request){
        $cari = $request->cari;
        
        $data = DB::table('id_kasus')
        ->where('nama', 'like', '%' .$cari. "%")
        ->orwhere('id', 'like', '%' .$cari. "%")
        ->paginate();

        return view('id_kasus.id_kasus', compact('data'));
    }



    // public function editAngkakematian($id ){
    //     $data = AngkakematianModel::where('id_kematian',$id)->first();

        
    //     return view('Angkakematian.edit', compact('data'));
    // }

    // public function updateAngkaKematian($id,Request $request ){
    //     $data =AngkakematianModel::where('id_kematian',$id)->update([
    //         'id' => $request->id,
    //         'id_kematian' => $request->id_kematian,
    //         'kasus' => $request->kasus,
    //         'tgl_kematian' => $request->tgl_kematian,
    //         'tkp' => $request->tkp,
    //     ]);

    //     return view('Angkakematian');
    // }

   
}
