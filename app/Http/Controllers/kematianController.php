<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\kematianModel;


class kematianController extends Controller
{
    public function kematian(){
        $data = DB::table('kematian')
        ->join ('orang','orang.id','=','kematian.id')
        ->select ('kematian.id_kematian','orang.id','orang.nama','kematian.kasus','kematian.tgl_kematian','kematian.tkp')
        ->where('kematian.is_active','1')
        ->get();
        // dd($data);

        return view('kematian.kematian',compact('data'));
    }

    public function TambahData(){
        return view('kematian.tambah');
    }

    public function postDataKematian(Request $request){
        $simpan = DB::table('kematian')->insert([
            'id_kematian' => $request->id_kematian,
            'id' => $request->id,
            'kasus' => $request->kasus,
            'tgl_kematian' => $request->tgl_kematian,
            'tkp' => $request->tkp,
            'is_active'=> 1,
        ]);
        // dd($simpan);

        if(!$simpan){
            return redirect()->route('kematian')->with('error','data gagal disimpan');
        }

        return redirect()->route('kematian')->with('success','data berhasil disimpan');
    }

    public function editkematian($id ){
        $data = kematianModel::where('id_kematian',$id)->first();

        
        return view('kematian.edit', compact('data'));
    }

    public function updateKematian($id,Request $request ){
        $data =kematianModel::where('id_kematian',$id)->update([
            'id' => $request->id,
            'id_kematian' => $request->id_kematian,
            'kasus' => $request->kasus,
            'tgl_kematian' => $request->tgl_kematian,
            'tkp' => $request->tkp,
        ]);

        return redirect('kematian');
    }

    public function softDelete($id,kematianModel $kematianModel){
        $kematianModel->where('id_kematian',$id)->update([
            'is_active'=>'0']
        );

        return redirect('kematian');
    }
}
