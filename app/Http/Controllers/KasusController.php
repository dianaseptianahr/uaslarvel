<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\KasusModel;

class KasusController extends Controller
{
    public function index(){
        $data = KasusModel::all()
            ->where('is_active','1') ;
        return view('kasus.kasus',compact('data'));
    }

    public function tambahkasus (){
        return view('kasus.TambahKasus');
    }
    public function posttambahkasus(Request $request){
        $simpan = DB::table('kasus')->insert([
            'id_kasus'=>$request->id_kasus,
            'nama_kasus'=>$request->nama_kasus,
            'is_active'=>1,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_kasus')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_kasus')->with('success','data berhasil disimpan');
    }

    public function editkasus($id ){
        $data = KasusModel::where('id_kasus',$id)->first();        
        return view('kasus.EditKasus', compact('data'));
    }

    public function update($id,Request $request ){
        $simpan =KasusModel::where('id_kasus',$id)->update([
            'id_kasus' => $request->id_kasus,
            'nama_kasus' => $request->nama_kasus,
        ]);
        if(!$simpan){
            return redirect()->route('tampil_kasus')->with('error','data gagal disimpan');
        }
        return redirect()->route('tampil_kasus')->with('succes','data berhasil disimpan');
        
    }

    public function softDelete($id,KasusModel $orangModel){
       $simpan= $orangModel->where('id_kasus',$id)->update([
            'is_active'=>'0']
        );

       if(!$simpan){
        return redirect()->route('tampil_kasus')->with('error','data gagal disimpan');
       }
       return redirect()->route('tampil_kasus')->with('succes','data berhasil disimpan');
    }

    public function CariData(Request $request){
        $cari = $request->cari;
        
        $data = DB::table('kasus')
        ->where('id_kasus', 'like', '%' .$cari. "%")
        ->orwhere('nama_kasus', 'like', '%' .$cari. "%")
        ->paginate();

        return view('kasus.kasus', compact('data'));
    }
}
