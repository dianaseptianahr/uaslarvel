<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\orangModel;


class orangController extends Controller

{
    //
    public function index(){
        return view ('admin.index');
    }
    public function dashboard(){

        return view('admin.dashboard');
    }

    public function orang(){
        $data = orangModel::all()
        ->where('is_active','1');
        // dd($data);

        return view('orang.orang',compact('data'));
    }
    public function TambahData(){
        return view('orang.tambah');
    }

    public function postData(Request $request){
        $simpan = DB::table('orang')->insert([
            'id' => $request->id,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
            'is_active'=> 1,
        ]);
        // dd($simpan);

        if(!$simpan){
            return redirect()->route('orang')->with('error','data gagal disimpan');
        }

        return redirect()->route('orang')->with('success','data berhasil disimpan');
    }

    public function editorang($id ){
        $data = orangModel::where('id',$id)->first();

        
        return view('orang.edit', compact('data'));
    }

    public function update($id,Request $request ){
        $data =orangModel::where('id',$id)->update([
            'id' => $request->id,
            'nama' => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tgl_lahir' => $request->tgl_lahir,
            'alamat' => $request->alamat,
        ]);

        return redirect('orang');
    }

    public function softDelete($id,orangModel $orangModel){
        $orangModel->where('id',$id)->update([
            'is_active'=>'0']
        );

        return redirect('orang');
    }

    public function CariData(Request $request){
        $cari = $request->cari;
        
        $data = DB::table('orang')
        ->where('nama', 'like', '%' .$cari. "%")
        ->orwhere('id', 'like', '%' .$cari. "%")
        ->paginate();

        return view('orang.orang', compact('data'));
    }
}
