<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kematianModel extends Model
{
    protected $table = 'kematian';
    protected $fillable = [
        'id_kematian','id','kasus','tgl_kematian','tkp','is_active',
    ];
}
