<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KasusModel extends Model
{
    protected $table = 'kasus';
    protected $fillable = [
        'id_kasus','nama_kasus','is_active',
    ];

    public function hasManyAngka (){

        return $this->hasMany(AngkakematianModel::class,'id_kasus','id_kasus');
    }
}
