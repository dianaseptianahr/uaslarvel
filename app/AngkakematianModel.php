<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AngkakematianModel extends Model
{
    protected $table = 'angkakematian';
    protected $fillable = [
        'id_angkakematian','id_kasus','jumlah','is_active',
    ];

    public function haveangka(){

        return $this->belongsTo(KasusModel::class, 'id_kasus','id_kasus');
    }
}
