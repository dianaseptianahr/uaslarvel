<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAngkakematianTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angkakematian', function (Blueprint $table) {
            $table->string('id_angkakematian')->primary();
            $table->string('id_kasus');
            $table->foreign('id_kasus')->references('id_kasus')->on('kasus');
            $table->string('jumlah');
            $table->tinyInteger('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angkakematian');
    }
}
