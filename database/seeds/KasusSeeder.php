<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KasusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kasus')->insert (array(
            array(
            'id_kasus' => '1',
            'nama_kasus' => 'kecelakaan',
            'created_at' => now(),
            'is_active'=> 1,
            ),
        ));
    }
}
