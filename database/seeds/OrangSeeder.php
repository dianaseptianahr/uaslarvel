<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orang')->insert ([
            'id' => '1',
            'nama' => 'Jack',
            'jenis_kelamin' => 'Laki-laki',
            'tgl_lahir' => now(),
            'alamat' => 'Bandung',
            'created_at' => now(),
            'is_active'=> 1,
        ]);

        DB::table('kematian')->insert ([
            'id_kematian' => '1',
            'id' => '1',
            'kasus' => 'bebas',
            'tgl_kematian' => now(),
            'tkp' => 'kali',
            'created_at' => now(),
            'is_active' => 1,
        ]);
    }
}
