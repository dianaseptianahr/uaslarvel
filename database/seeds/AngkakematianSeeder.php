<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AngkakematianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('angkakematian')->insert (array(
            array(
            'id_angkakematian' => '1',
            'id_kasus' => '1',
            'jumlah' => '200',
            'created_at' => now(),
            'is_active'=> 1,
        ),
        ));
    }
}
