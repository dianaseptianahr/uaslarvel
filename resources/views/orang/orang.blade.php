@extends('template_admin.master')

@section('content')
<div class="content-wrapper">
          
         
          <div class="row" id="proBanner">
            <div class="col-md-12 grid-margin">
              <div class="card bg-gradient-primary border-0">
                
              </div>
            </div>
          </div>
         
          <!-- <div class="row">
            <div class="col-md-7 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Cash deposits</p>
                  <p class="mb-4">To start a blog, think of a topic about and first brainstorm party is ways to write details</p>
                  <div id="cash-deposits-chart-legend" class="d-flex justify-content-center pt-3"></div>
                  <canvas id="cash-deposits-chart"></canvas>
                </div>
              </div>
            </div>
            <div class="col-md-5 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Total sales</p>
                  <h1>$ 28835</h1>
                  <h4>Gross sales over the years</h4>
                  <p class="text-muted">Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important </p>
                  <div id="total-sales-chart-legend"></div>                  
                </div>
                <canvas id="total-sales-chart"></canvas>
              </div>
            </div>
          </div> -->
          <form class="navbar-form col-md-3 float-right" method="get" action="{{route('CariData')}}">
                            <div class="input-group no-border">
                                <input type="text" name="cari" value="" class="form-control " placeholder="CariData...">
                                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  		 <i class="mdi mdi-magnify"></i>

                  <div class="ripple-container"></div>
                </button>
            </div>
            </form>
            <a href="{{route('TambahData')}}"class="btn btn-primary">Tambah</a><br><br>
          <div class="row">
            <div class="col-md-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-title">Orang</p>
                  <div class="table-responsive">
                    <table id="recent-purchases-listing" class="table">
                      <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tgl Lahir</th>
                            <th>Alamat</th>
                            <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          @foreach($data as $row)
                        <tr>
                            <td>{{$row->id}}</td>
                            <td>{{$row->nama}}</td>
                            <td>{{$row->jenis_kelamin}}</td>
                            <td>{{$row->tgl_lahir}}</td>
                            <td>{{$row->alamat}}</td>
                            <td>
                                <a href="{{route('Edit_orang',$row->id)}}"class="btn btn-primary">Edit</a>
                                <a href="{{route('softdelete',$row->id)}}"class="btn btn-primary">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection