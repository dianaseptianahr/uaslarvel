<style>
  .form {
    position: absolute;
    width: 100vw;
    height: 100vh;
    background: linear-gradient(to bottom right, teal, cyan, teal, teal);
    z-index: 99999;
    padding: 40px;
  }

  .form-login {
    position: relative;
    left: 50%;
    transform: translate(-50%);
    width: 30%;
    height: auto;
    background: rgba(30, 30, 30, 0.5);
    border-radius: 20px;
    display: flex;
    align-items: center;
    flex-direction: column;
    padding: -0 30px 20px 30px;
  }

  .header-login {
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    color: azure;
    width: 100%;
    text-align: center;
    padding: 20px;
    background: rgba(0, 0, 0, 0.4);

  }

  input[type=text], input[type=password], .btn-login {
    width: 90%;
    height: 30px;
    margin-bottom: 10px;
    border-radius: 5px;
    border: none;
    padding: 15px;
    box-sizing: border-box;
  }
  
  .btn-login {
    background: azure;
    border: none;
    outline: none;
    box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
    color: teal;
    font-weight: bold;
    padding: 5px;
  }


  .popup-box {
    position: fixed;
    top: 10%;
    visibility: hidden;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    width: 20%;
    left: 50%;
    transform: translate(-50%);
    height: 20%;
    background: azure;
    border-radius: 20px;
    box-shadow: 5px 5px 5px rgba(0, 0, 0, 0.5);
  }

  .popup-text {
    color: teal;
  }

  .x {
    position: absolute;
    right: -4px;
    top: -4px;
    border: none;
    border-radius: 4px;
    box-shadow: 3px 3px 3px rgba(0, 0, 0, 0.5);
    display: flex;
    justify-content: center;
    align-items: center;
    color: teal;
    padding: 10px;
  }
</style>


<div id="formL" class="form">
<div class="form-login">
    <h1 class="header-login">Data Kematian</h1>
    <input id="user" type="text" placeholder="Nama Pengguna">
    <br>
    <input id="pass" type="password" placeholder="Password">
    <br>
    <button id="btnLogin" onclick="login()" class="btn-login">Login</button>
  </div>

<div class="popup-box" id="popBox">
    <button onclick="closePopup()" class="x">X</button>
    <h5 id="popText" class="popup-text"></h5>
</div>

</div>

<script>
  var user = document.getElementById('user');
  var pass = document.getElementById('pass');
  var formL = document.getElementById('formL');
  var box = document.getElementById('popBox');
  var tBox = document.getElementById('popText')
  function login(){
    if(user.value == "admin" && pass.value == "adm2020"){
      alert("login berhasil");
      formL.style.visibility = "hidden";  
    }

    else {
      tBox.innerHTML = "Nama Pengguna / Password salah";
      box.style.visibility = "visible";
      
    }
  }

  function closePopup(){
    box.style.visibility = "hidden";
    user.value = "";
    pass.value = "";
  }
</script>


@extends('template_admin.master')

@section('content')
<div class="card">

                <br></br><br></br><br></br><br></br><br></br><br></br>
                  <h5 class="judul-aplikasi" style="font-size:80px" ml-5>Aplikasi Data Kematian</h5>
                </div><br></br><br></br><br></br><br></br><br></br>
                @endsection