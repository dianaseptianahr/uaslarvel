@extends('template_admin.master')

@section('content')
<div class="card">
                  <div class="card-header">
                    <h4>Tabel Edit</h4>
                  </div>
                  <div class="card-body">
                    <form action="{{route('update_editKematian',$data->id_kematian)}}" method="post">
                     @csrf
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id Kematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id_kematian" value="{{$data->id_kematian}}">
                      </div>
                    </div>
                    <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">id</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id" value="{{$data->id}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Kasus</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="kasus" value="{{$data->kasus}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Tgl Kematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="date" class="form-control" name="tgl_kematian" value="{{$data->tgl_kematian}}">
                      </div>
                    </div>
                    <div class="card-body">
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">TKP</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="tkp" value="{{$data->tkp}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button href =""class="btn btn-primary">Tambah</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
                @endsection