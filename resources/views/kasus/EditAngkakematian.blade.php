@extends('template_admin.master')

@section('content')
<div class="card">
                  <div class="card-header">
                    <h4>Tabel Edit Angka Kematian</h4>
                  </div>
                  <div class="card-body">
                    <form action="{{route('update_editAngkaKematian',$data->id_angkakematian)}}" method="post">
                     @csrf
                     <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id angkakematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id_angkakematian" value="{{$data->id_angkakematian}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Id Kasus</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="id_kasus" value="{{$data->id_kasus}}">
                      </div>
                    </div>
                    <div class="card-body">
                    <!-- <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Faktor Kematian</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="faktor_kematian" value="{{$data->faktor_kematian}}">
                      </div>
                    </div> -->
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Jumlah</label>
                      <div class="col-sm-12 col-md-7">
                        <input type="text" class="form-control" name="jumlah" value="{{$data->jumlah}}">
                      </div>
                    </div>
                    <div class="form-group row mb-4">
                      <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                      <div class="col-sm-12 col-md-7">
                        <button href =""class="btn btn-primary">Tambah</button>
                      </div>
                    </div>
                    </form>
                  </div>
                </div>
                @endsection